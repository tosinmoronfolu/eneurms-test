import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Schema as ms } from 'mongoose';

export type StoreDocument = Store & Document;

@Schema({ timestamps: true })
export class Store {
  @Prop({ required: true, ref: 'business', type: ms.Types.ObjectId })
  businessId: ObjectId;

  @Prop({ unique: true, required: true })
  storeName: string;

  @Prop({ required: true })
  industry: string;

  @Prop({ required: true })
  contactEmail: string;

  @Prop({ required: true })
  senderEmail: string;

  @Prop({ required: true })
  contactPhone: number;

  @Prop({ required: true })
  address: string;

  @Prop({ ref: 'standard', type: ms.Types.ObjectId })
  standardId: ObjectId;

  @Prop({ required: true, ref: 'currency', type: ms.Types.ObjectId })
  currencyId: ObjectId;

  @Prop({ ref: 'socialmedia', type: ms.Types.ObjectId })
  socialMediaId: ObjectId;

  @Prop({ enum: ['open', 'closed'], default: 'closed' })
  status: string;
}

export const StoreSchema = SchemaFactory.createForClass(Store);

// user store schema
export type UserStoreDocument = UserStore & Document;

@Schema({ timestamps: true })
export class UserStore {
  @Prop({ ref: 'store', type: ms.Types.ObjectId, required: true })
  storeId: string;

  @Prop({ required: true, ref: 'user', type: ms.Types.ObjectId })
  userId: ObjectId;

  @Prop({ required: true })
  role: string;
}

export const UserStoreSchema = SchemaFactory.createForClass(UserStore);
