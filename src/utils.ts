import { compareSync, hashSync } from 'bcryptjs';

export const sendResponse = (
  res,
  statusCode = 200,
  result: Response
): Response => {
  const { status } = result;

  if (status == 'failed') statusCode = 400;

  return res.status(statusCode).json(result);
};

export class Response {
  status: string;
  message: string;
  data: any;
}

export const hashPassword = (password: string) => {
  return hashSync(password, 10);
};

export const comparePassword = (password, hashedPassword) => {
  return compareSync(password, hashedPassword);
};

export const createToken = (chars: number) => {
  return Math.floor(Math.random() * Math.pow(10, chars));
};

// export const generateFreshUserTokens = async (user: object) => {
//   const tokenExpiryInSeconds = parseInt(
//     process.env.JWT_TOKEN_EXPIRY_IN_SECONDS,
//     10
//   );
//   const tokenExpiresAt = new Date(
//     new Date().getTime() + 1000 * tokenExpiryInSeconds
//   );
//   const accessToken = createToken(
//     { ...user, tokenExpiresAt },
//     tokenExpiryInSeconds
//   );

//   return { accessToken };
// };
