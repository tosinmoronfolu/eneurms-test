import { IsOptional, IsUrl } from 'class-validator';

export class CreateSocialMediaDTO {
  @IsOptional()
  @IsUrl()
  facebook?: string;

  @IsOptional()
  @IsUrl()
  twitter?: string;

  @IsOptional()
  @IsUrl()
  youtube?: string;

  @IsOptional()
  @IsUrl()
  Instagram?: string;

  @IsOptional()
  @IsUrl()
  pinterest?: string;
}
