import { UpdateUserDTO } from './../dtos/user.dto';
import { UserDocument } from '../schemas/user.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from 'src/schemas/user.schema';
import { Model } from 'mongoose';
import { Response } from 'src/utils';

Injectable();
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>
  ) {}

  async findOne(email: string): Promise<Response> {
    const user = await this.userModel.findOne({ email }).lean();

    if (!user)
      return {
        status: 'failed',
        message: 'user account does not exist',
        data: null,
      };

    return {
      status: 'success',
      message: '',
      data: { ...user, userId: user._id },
    };
  }

  async updateProfile(email: string, data: UpdateUserDTO): Promise<Response> {
    const user = await this.userModel.findOne({ email }).lean();

    if (!user)
      return {
        status: 'failed',
        message: 'user account does not exist',
        data: null,
      };

    const updatedUser = await this.userModel
      .findOneAndUpdate({ email }, { $set: { data } }, { new: true })
      .lean();

    return {
      status: 'success',
      message: 'user profile updated successfully',
      data: { ...updatedUser },
    };
  }
}
