import { ObjectId } from 'mongoose';
import { JwtAuthGuard } from './../guards/jwt-auth.guard';
import { CreateStoreDTO } from './../dtos/store.dto';
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Request,
  Response,
  UseGuards,
} from '@nestjs/common';
import { StoreService } from './../services/store.service';
import { Response as SendResponse, sendResponse } from 'src/utils';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';

@UseGuards(JwtAuthGuard)
@ApiTags('stores')
@ApiBearerAuth()
@Controller('stores')
export class StoreController {
  constructor(private readonly storeService: StoreService) {}

  @Post('')
  async createStore(
    @Body() body: CreateStoreDTO,
    @Request() req,
    @Response() res
  ): Promise<SendResponse> {
    const userId = req.user.userId;

    return sendResponse(
      res,
      201,
      await this.storeService.createStore(body, userId)
    );
  }

  @ApiParam({
    name: 'storeId',
    description: 'This is the ID of a specific store',
  })
  @Put(':storeId')
  async updateStore(
    @Body() body: CreateStoreDTO,
    @Request() req,
    @Response() res,
    @Param('storeId') storeId: ObjectId
  ): Promise<SendResponse> {
    const userId = req.user.userId;

    return sendResponse(
      res,
      201,
      await this.storeService.updateStore(body, userId, storeId)
    );
  }

  @ApiParam({
    name: 'storeId',
    description: 'This is the ID of a specific store',
  })
  @Get(':storeId')
  async getStore(
    @Request() req,
    @Response() res,
    @Param('storeId') storeId: ObjectId
  ): Promise<SendResponse> {
    return sendResponse(res, 200, await this.storeService.findOne(storeId));
  }

  @Get('user-stores')
  async getUserStores(@Request() req, @Response() res): Promise<SendResponse> {
    const userId = req.user.userId;

    return sendResponse(
      res,
      200,
      await this.storeService.getUserStores(userId)
    );
  }
}
