import { ObjectId } from 'mongoose';
import { IsMongoId, IsNotEmpty, IsNumber } from 'class-validator';

export class AddToCartDTO {
  @IsNotEmpty()
  @IsMongoId()
  productId: ObjectId;

  @IsNotEmpty()
  @IsNumber()
  price: number;

  @IsNotEmpty()
  @IsNumber()
  quantity: number;

  @IsNotEmpty()
  @IsNumber()
  subTotal: number;
}
