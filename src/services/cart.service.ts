import { Product, ProductDocument } from './../schemas/product.schema';
import { Cart, CartDocument } from './../schemas/cart.schema';
import { AddToCartDTO } from './../dtos/cart.dto';
import { Injectable } from '@nestjs/common';
import { Response } from 'src/utils';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';

@Injectable()
export class CartService {
  constructor(
    @InjectModel(Cart.name) private readonly cartModel: Model<CartDocument>,
    @InjectModel(Product.name)
    private readonly productModel: Model<ProductDocument>
  ) {}

  async addToCart(details: AddToCartDTO, userId: ObjectId): Promise<Response> {
    const product = await this.productModel.findOne({ _id: details.productId });

    if (!product)
      return {
        status: 'failed',
        message: 'product does not exist',
        data: null,
      };

    await this.cartModel.create({ ...details, userId });

    const userCart = await this.cartModel.find({ userId }).lean();

    return {
      status: 'success',
      message: 'product successfully added to cart',
      data: { ...userCart },
    };
  }

  async updateProductInCart(
    body: AddToCartDTO,
    userId: ObjectId
  ): Promise<Response> {
    const product = await this.productModel.findOne({ _id: body.productId });

    if (!product)
      return {
        status: 'failed',
        message: 'product does not exist',
        data: null,
      };

    await this.cartModel.findOneAndUpdate(
      { productId: body.productId, userId },
      { $set: { ...body } },
      { new: true }
    );

    const userCart = await this.cartModel.find({ userId }).lean();

    return {
      status: 'success',
      message: 'product successfully updated in cart',
      data: { ...userCart },
    };
  }

  async getUserCart(userId: ObjectId): Promise<Response> {
    const cart = await this.cartModel.find({ userId }).lean();

    if (!cart)
      return {
        status: 'failed',
        message: 'no product in cart',
        data: null,
      };

    return {
      status: 'success',
      message: '',
      data: { cart },
    };
  }

  async deleteProductInCart(
    productId: ObjectId,
    userId: ObjectId
  ): Promise<Response> {
    const productInCart = await this.cartModel
      .find({ userId, productId })
      .lean();

    if (!productInCart)
      return {
        status: 'failed',
        message: 'product not in cart',
        data: null,
      };

    await this.cartModel.findOneAndDelete({ userId, productId });

    return {
      status: 'success',
      message: 'product deleted from cart',
      data: null,
    };
  }
}
