import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Schema as ms } from 'mongoose';

export type SocialMediaAccountDocument = SocialMediaAccount & Document;

@Schema({ timestamps: true })
export class SocialMediaAccount {
  @Prop({ required: true, type: ms.Types.ObjectId, ref: 'store' })
  storeId: ObjectId;

  @Prop()
  facebook: string;

  @Prop()
  youtube: string;

  @Prop()
  instagram: string;

  @Prop()
  pinterest: string;

  @Prop()
  twitter: string;
}

export const SocialMediaAccountSchema =
  SchemaFactory.createForClass(SocialMediaAccount);
