import { CartService } from './../services/cart.service';
import { CartController } from './../controllers/cart.controller';
import { Product, ProductSchema } from './../schemas/product.schema';
import { Cart, CartSchema } from './../schemas/cart.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Cart.name, schema: CartSchema },
      { name: Product.name, schema: ProductSchema },
    ]),
  ],
  providers: [CartService],
  controllers: [CartController],
  exports: [],
})
export class CartModule {}
