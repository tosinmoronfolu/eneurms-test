import { ObjectId } from 'mongoose';
import { BusinessService } from './../services/business.service';
import { CreateBusinessDTO } from './../dtos/business.dto';
import { JwtAuthGuard } from './../guards/jwt-auth.guard';
import {
  Body,
  Controller,
  Param,
  Post,
  Put,
  Request,
  Response,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Response as SendResponse, sendResponse } from 'src/utils';

@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@ApiTags('Businesses')
@Controller('businesses')
export class BusinessController {
  constructor(private readonly businessService: BusinessService) {}

  @Post('')
  async createBusiness(
    @Request() req,
    @Body() body: CreateBusinessDTO,
    @Response() res
  ): Promise<SendResponse> {
    const businessOwner = req.user.userId;

    return sendResponse(
      res,
      201,
      await this.businessService.createBusiness(body, businessOwner)
    );
  }

  @Put(':businessId')
  async updateBusiness(
    @Request() req,
    @Body() body: CreateBusinessDTO,
    @Response() res,
    @Param('businessId') businessId: ObjectId
  ): Promise<SendResponse> {
    const businessOwner = req.user.userId;

    return sendResponse(
      res,
      200,
      await this.businessService.updateBusiness(body, businessId, businessOwner)
    );
  }
}
