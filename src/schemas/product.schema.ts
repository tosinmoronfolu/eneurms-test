import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Schema as ms } from 'mongoose';

export type ProductDocument = Product & Document;

@Schema({ timestamps: true })
export class Product {
  @Prop({ ref: 'store', type: ms.Types.ObjectId })
  storeId: ObjectId;

  @Prop({ required: true })
  name: string;

  @Prop({ type: Date })
  description: string;

  @Prop({ required: true })
  regularPrice: number;

  @Prop({ required: true })
  salePrice: number;

  @Prop()
  costPrice: number;

  @Prop()
  wholeSalePrice: number;

  @Prop()
  sku: string;

  @Prop()
  barcode: string;

  @Prop()
  quantity: number;

  @Prop({ required: true, default: false })
  trackQuantity: boolean;

  @Prop({ required: true, default: false })
  sellOutOfStock: boolean;

  @Prop({ required: false })
  options: [{ [key: string]: [string | number] }];
}

export const ProductSchema = SchemaFactory.createForClass(Product);
