import {
  SocialMediaAccount,
  SocialMediaAccountDocument,
} from './../schemas/socials.schema';
import {
  StandardFormat,
  StandardFormatDocument,
} from './../schemas/standardFormat.schema';
import { Business, BusinessDocument } from 'src/schemas/business.schema';
import { CreateStoreDTO } from './../dtos/store.dto';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { Store, StoreDocument } from 'src/schemas/store.schema';
import { Response } from 'src/utils';

@Injectable()
export class StoreService {
  constructor(
    @InjectModel(Store.name) private readonly storeModel: Model<StoreDocument>,
    @InjectModel(Business.name)
    private readonly businessModel: Model<BusinessDocument>,
    @InjectModel(StandardFormat.name)
    private readonly standardFormatModel: Model<StandardFormatDocument>,
    @InjectModel(SocialMediaAccount.name)
    private readonly socialMediaAccountModel: Model<SocialMediaAccountDocument>
  ) {}

  async createStore(
    storeData: CreateStoreDTO,
    userId: ObjectId
  ): Promise<Response> {
    const storeNameTaken = await this.getStoreByName(storeData.storeName);

    if (storeNameTaken.status == 'success')
      return {
        status: 'failed',
        message: 'store name already taken',
        data: null,
      };

    const business = await this.businessModel.findOne({
      businessOwner: userId,
    });

    if (!business)
      return {
        status: 'failed',
        message: 'you do not have a business account',
        data: null,
      };

    const store = await this.storeModel.create({
      ...storeData,
      businessId: business._id,
    });

    if (storeData.standard) {
      const sf = await this.standardFormatModel.create({
        ...storeData.standard,
        storeId: store._id,
      });

      await this.storeModel.findOneAndUpdate(
        { _id: store._id },
        { $set: { standardId: sf._id } },
        { new: true }
      );
    }

    if (storeData.socialMedia) {
      const sm = await this.socialMediaAccountModel.create({
        ...storeData.socialMedia,
        storeId: store._id,
      });

      await this.storeModel.findOneAndUpdate(
        { _id: store._id },
        { $set: { socialMediaId: sm._id } },
        { new: true }
      );
    }

    return {
      status: 'success',
      message: 'store created successfully',
      data: store,
    };
  }

  async updateStore(
    storeData: CreateStoreDTO,
    userId: ObjectId,
    storeId: ObjectId
  ): Promise<Response> {
    const store = await this.storeModel.findOne({ _id: storeId }).lean();

    if (!store)
      return {
        status: 'failed',
        message: 'store does not exist',
        data: null,
      };

    const storeNameTaken = await this.getStoreByName(storeData.storeName);

    if (storeNameTaken.status == 'success')
      return {
        status: 'failed',
        message: 'store name already taken',
        data: null,
      };

    const business = await this.businessModel.findOne({
      businessOwner: userId,
    });

    if (!business)
      return {
        status: 'failed',
        message: 'you do not have a business account',
        data: null,
      };

    const updatedStore = await this.storeModel.findOneAndUpdate(
      { _id: storeId },
      { $set: { ...storeData, businessId: business._id } },
      { new: true }
    );

    if (storeData.standard) {
      const sf = await this.standardFormatModel.findOneAndUpdate(
        { storeId },
        { $set: { ...storeData.standard, storeId } },
        { new: true }
      );

      await this.storeModel.findOneAndUpdate(
        { _id: updatedStore._id },
        { $set: { standardId: sf._id } },
        { new: true }
      );
    }

    if (storeData.socialMedia) {
      const sm = await this.socialMediaAccountModel.findOneAndUpdate(
        { storeId },
        { $set: { ...storeData.socialMedia, storeId } },
        { new: true }
      );

      await this.storeModel.findOneAndUpdate(
        { _id: updatedStore._id },
        { $set: { socialMediaId: sm._id } },
        { new: true }
      );
    }

    return {
      status: 'success',
      message: 'store updated successfully',
      data: updatedStore,
    };
  }

  async getStoreByName(storeName: string): Promise<Response> {
    const store = await this.storeModel.findOne({ storeName });

    if (store)
      return {
        status: 'success',
        message: '',
        data: store,
      };

    return {
      status: 'failed',
      message: 'store does not exist',
      data: null,
    };
  }

  async findOne(storeId: ObjectId): Promise<Response> {
    const store = await this.storeModel.findOne({ _id: storeId }).lean();

    if (!store)
      return {
        status: 'failed',
        message: 'store does not exist',
        data: null,
      };

    return {
      status: 'success',
      message: '',
      data: store,
    };
  }

  async getUserStores(userId: ObjectId): Promise<Response> {
    const userBusiness = await this.businessModel.findOne({
      businessOwner: userId,
    });

    if (!userBusiness)
      return {
        status: 'failed',
        message: 'you do not have a business account',
        data: null,
      };

    const stores = await this.storeModel.find({ businessId: userBusiness._id });

    if (!stores)
      return {
        status: 'failed',
        message: 'you have not created any store',
        data: null,
      };

    return {
      status: 'success',
      message: '',
      data: stores,
    };
  }
}
