import { CreateSocialMediaDTO } from './../dtos/socialmedia.dto';
import {
  SocialMediaAccount,
  SocialMediaAccountDocument,
} from './../schemas/socials.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { Response } from 'src/utils';

@Injectable()
export class SocialsService {
  constructor(
    @InjectModel(SocialMediaAccount.name)
    private readonly socialMediaAccountModel: Model<SocialMediaAccountDocument>
  ) {}

  async createSocialAccount(
    data: CreateSocialMediaDTO,
    storeId: ObjectId
  ): Promise<Response> {
    const socialAccount = await this.socialMediaAccountModel.create({
      ...data,
      storeId,
    });

    return {
      status: 'success',
      message: '',
      data: socialAccount,
    };
  }
}
