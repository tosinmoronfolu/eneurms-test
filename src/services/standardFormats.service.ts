import { StoreDocument } from './../schemas/store.schema';
import { Store } from 'src/schemas/store.schema';
import { StoreService } from './store.service';
import { CreateStandardDTO } from './../dtos/standard.dto';
import {
  StandardFormat,
  StandardFormatDocument,
} from './../schemas/standardFormat.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { Response } from 'src/utils';

Injectable();
export class StandardFormatService {
  constructor(
    @InjectModel(StandardFormat.name)
    private readonly standardFormatModel: Model<StandardFormatDocument>,
    @InjectModel(Store.name) private readonly storeModel: Model<StoreDocument>
  ) {}

  async create(
    details: CreateStandardDTO,
    storeId: ObjectId
  ): Promise<Response> {
    const store = await this.storeModel.findOne({ _id: storeId });

    if (!store)
      return {
        status: 'failed',
        message: 'store does not exist',
        data: null,
      };

    const sf = await this.standardFormatModel.create({
      ...details,
      storeId,
    });

    if (!sf)
      return {
        status: 'failed',
        message: 'user account does not exist',
        data: null,
      };

    return {
      status: 'success',
      message: '',
      data: sf,
    };
  }
}
