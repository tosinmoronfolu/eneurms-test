import { Business, BusinessSchema } from './../schemas/business.schema';
import { BusinessService } from './../services/business.service';
import { BusinessController } from './../controllers/business.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Business.name, schema: BusinessSchema },
    ]),
  ],
  providers: [BusinessService],
  controllers: [BusinessController],
  exports: [],
})
export class BusinessModule {}
