import { Injectable } from '@nestjs/common';
import * as sgMail from '@sendgrid/mail';

@Injectable()
export class MailService {
  constructor() {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  }

  async sendMail(
    toe: string,
    from: string,
    subject: string,
    text: string
  ): Promise<string> {
    const msg = {
      to: { name: 'tosin', email: toe },
      from,
      subject,
      text,
      templateId: process.env.SENDGRID_TEMPLATE_ID,
      substitutions: { name: 'tosin' },
    };

    const result = await sgMail.send(msg);

    return result.toString();
  }
}
