import { CreateStandardDTO } from './standard.dto';
import { CreateSocialMediaDTO } from './socialmedia.dto';
import { ObjectId } from 'mongoose';
import {
  IsEmail,
  IsEnum,
  IsMongoId,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from 'class-validator';

export class CreateStoreDTO {
  @IsNotEmpty()
  @IsString()
  storeName: string;

  @IsNotEmpty()
  @IsString()
  industry: string;

  @IsNotEmpty()
  @IsEmail()
  contactEmail: string;

  @IsNotEmpty()
  @IsEmail()
  senderEmail: string;

  @IsNotEmpty()
  @IsPhoneNumber()
  contactPhone: number;

  @IsNotEmpty()
  @IsString()
  address: string;

  @IsOptional()
  @IsObject()
  standard?: CreateStandardDTO;

  @IsNotEmpty()
  @IsMongoId()
  currencyId: ObjectId;

  @IsOptional()
  @IsObject()
  socialMedia?: CreateSocialMediaDTO;

  @IsOptional()
  @IsEnum(['open', 'closed'])
  status?: string;
}
