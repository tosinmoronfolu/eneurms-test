import { CartModule } from './modules/cart.module';
import { ProductModule } from './modules/product.module';
import { StoreModule } from './modules/store.module';
import { BusinessModule } from './modules/business.module';
import { AuthModule } from './modules/auth.module';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MongooseModule.forRoot(
      `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.4vdlm.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`
    ),
    AuthModule,
    BusinessModule,
    StoreModule,
    ProductModule,
    CartModule,
  ],
})
export class AppModule {}
