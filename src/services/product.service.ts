import { Store, StoreDocument } from 'src/schemas/store.schema';
import { CreateProductDTO } from './../dtos/product.dto';
import { Product, ProductDocument } from './../schemas/product.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { Response } from 'src/utils';

@Injectable()
export class ProductService {
  constructor(
    @InjectModel(Product.name) private productModel: Model<ProductDocument>,
    @InjectModel(Store.name) private storeModel: Model<StoreDocument>
  ) {}

  async createProduct(
    details: CreateProductDTO,
    storeId: ObjectId
  ): Promise<Response> {
    const store = await this.storeModel.findOne({ _id: storeId });

    if (!store)
      return {
        status: 'failed',
        message: 'store does not exist',
        data: null,
      };

    const product = await this.productModel.create({ ...details, storeId });

    return {
      status: 'success',
      message: '',
      data: product,
    };
  }

  async editProduct(
    productId: ObjectId,
    details: CreateProductDTO
  ): Promise<Response> {
    const product = await this.productModel.findOne({ _id: productId });

    if (!product)
      return {
        status: 'failed',
        message: 'product does not exist',
        data: null,
      };

    const updatedProduct = await this.productModel.findOneAndUpdate(
      { _id: productId },
      { $set: { ...details } },
      { new: true }
    );

    return {
      status: 'success',
      message: '',
      data: updatedProduct,
    };
  }

  async findOne(productId: ObjectId): Promise<Response> {
    const product = await this.productModel.findOne({ _id: productId });

    if (!product)
      return {
        status: 'failed',
        message: 'product does not exist',
        data: null,
      };

    return {
      status: 'success',
      message: '',
      data: product,
    };
  }

  async deleteProduct(productId: ObjectId): Promise<Response> {
    const product = await this.productModel.findOne({ _id: productId });

    if (!product)
      return {
        status: 'failed',
        message: 'product does not exist',
        data: null,
      };

    await this.productModel.deleteOne({ _id: productId });

    return {
      status: 'success',
      message: 'product deleted successfully',
      data: null,
    };
  }
}
