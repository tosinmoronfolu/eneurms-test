import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Schema as ms } from 'mongoose';

export type TokenDocument = Token & Document;

@Schema({ timestamps: true })
export class Token {
  @Prop({ ref: 'user', type: ms.Types.ObjectId })
  userId: ObjectId;

  @Prop({ required: true })
  token: string;

  @Prop({ type: Date })
  expiresAt: string;

  @Prop({ type: Boolean, default: false })
  isExpired: boolean;
}

export const TokenSchema = SchemaFactory.createForClass(Token);
