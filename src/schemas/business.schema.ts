import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Schema as ms } from 'mongoose';

export type BusinessDocument = Business & Document;

@Schema({ timestamps: true })
export class Business {
  @Prop({ unique: true, required: true, type: String })
  businessName: string;

  @Prop({ required: true, ref: 'user', type: ms.Types.ObjectId })
  businessOwner: ObjectId;
}

export const BusinessSchema = SchemaFactory.createForClass(Business);
