import { ObjectId } from 'mongoose';
import { JwtAuthGuard } from './../guards/jwt-auth.guard';
import { GoogleOAuthGuard } from './../guards/google-oauth.guard';
import { LocalAuthGuard } from './../guards/local-auth.guard';
import { AuthService } from '../services/auth.service';
import {
  CreateUserDTO,
  ForgotPasswordDTO,
  LoginUserDTO,
  RefreshTokenDTO,
  ResetPasswordDTO,
} from './../dtos/user.dto';
import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Post,
  Query,
  Request,
  Response,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Response as SendResponse, sendResponse } from 'src/utils';
import { ApiTags } from '@nestjs/swagger';
import * as express from 'express';
import { FacebookOAuthGuard } from 'src/guards/facebook-login.guard';

@ApiTags('Authentication')
@UseInterceptors(ClassSerializerInterceptor)
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  /* 
    A user creates an account
  */
  @Post('register')
  async register(
    @Body() data: CreateUserDTO,
    @Response() res
  ): Promise<SendResponse> {
    return sendResponse(res, 201, await this.authService.register(data));
  }

  /* 
    A user logs into their account
  */
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(
    @Request() req,
    @Body() body: LoginUserDTO,
    @Response() res
  ): Promise<SendResponse> {
    return sendResponse(res, 200, await this.authService.login(req.user));
  }

  /**
   *
   * @param body
   * @param res
   * @returns {SendResponse}
   */
  @Post('forgot-password')
  async forgotPassword(
    @Body() body: ForgotPasswordDTO,
    @Response() res
  ): Promise<SendResponse> {
    return sendResponse(res, 200, await this.authService.forgotPassword(body));
  }

  /**
   *
   * @param body
   * @param token
   * @param res
   * @returns {SendResponse}
   */

  @Post('reset-password')
  async resetPassword(
    @Body() body: ResetPasswordDTO,
    @Query('reset_password_token') token: string,
    @Response() res
  ): Promise<SendResponse> {
    return sendResponse(
      res,
      200,
      await this.authService.resetPassword(body, token)
    );
  }

  /**
   *
   * @param body
   * @param token
   * @param res
   * @returns {SendResponse}
   */

  @Post('activate-account')
  async activateAccount(
    @Query('token') token: string,
    @Response() res
  ): Promise<SendResponse> {
    return sendResponse(
      res,
      200,
      await this.authService.activateAccount(token)
    );
  }

  /**
   *
   * @param body
   * @param token
   * @param res
   * @param req
   * @returns
   */
  @UseGuards(JwtAuthGuard)
  @Get('resend-verification-token')
  async resendToken(
    @Body() body: ResetPasswordDTO,
    @Query('token') token: string,
    @Response() res,
    @Request() req
  ): Promise<SendResponse> {
    return sendResponse(
      res,
      200,
      await this.authService.resendToken(req.user.id, req.user.email)
    );
  }

  @Get('google-login')
  @UseGuards(GoogleOAuthGuard)
  async googleAuth(@Request() req) {}

  @Get('google-redirect')
  @UseGuards(GoogleOAuthGuard)
  async googleAuthRedirect(
    @Request() req: express.Request,
    @Response() res: express.Response
  ): Promise<any> {
    const { status, data } = await this.authService.googleLogin(req);

    return res.redirect(`http://localhost:8600?token=${data.accessToken}`);
  }

  @Get('facebook-login')
  @UseGuards(FacebookOAuthGuard)
  async facebookAuth(@Request() req) {
    console.log(req.user);
  }

  @Get('facebook-redirect')
  @UseGuards(FacebookOAuthGuard)
  async faceAuthRedirect(@Request() req, @Response() res: express.Response) {
    return res.send(req.user);
  }

  @Post('refresh-token')
  async refreshAccessToken(
    @Body() body: RefreshTokenDTO,
    @Response() res
  ): Promise<SendResponse> {
    return sendResponse(
      res,
      200,
      await this.authService.refreshAccessToken(body.userId, body.refreshToken)
    );
  }
}
