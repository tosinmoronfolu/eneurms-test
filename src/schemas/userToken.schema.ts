import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as ms, ObjectId } from 'mongoose';

const tokenExpiry = () => {
  return new Date(new Date().getTime() + 1000 * 60 * 60 * 24);
};

export type UserTokenDocument = UserToken & Document;

@Schema({ timestamps: true })
export class UserToken {
  @Prop({ ref: 'user', required: true, type: ms.Types.ObjectId })
  userId: ObjectId;

  @Prop({ required: true })
  accessToken: string;

  @Prop({ required: true })
  refreshToken: string;

  @Prop({ type: Date, default: tokenExpiry })
  expiresAt: Date;

  @Prop({ type: Boolean, default: false })
  isExpired: string;
}

export const UserTokenSchema = SchemaFactory.createForClass(UserToken);
