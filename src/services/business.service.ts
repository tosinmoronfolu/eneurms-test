import { CreateBusinessDTO } from './../dtos/business.dto';
import { Business, BusinessDocument } from './../schemas/business.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { Response } from 'src/utils';

Injectable();
export class BusinessService {
  constructor(
    @InjectModel(Business.name)
    private readonly businessModel: Model<BusinessDocument>
  ) {}

  async createBusiness(
    businessData: CreateBusinessDTO,
    businessOwner: ObjectId
  ): Promise<Response> {
    const businessNameTaken = await this.findBusinessByName(
      businessData.businessName
    );

    if (businessNameTaken.status == 'success')
      return {
        status: 'failed',
        message: 'business name already taken',
        data: null,
      };

    const createdBusiness = await this.businessModel.create({
      businessName: businessData.businessName,
      businessOwner,
    });

    const data = {
      businessName: createdBusiness.businessName,
      businessOwner: createdBusiness.businessOwner,
      businessId: createdBusiness._id,
    };

    return {
      status: 'success',
      message: 'business created successfully',
      data,
    };
  }

  async updateBusiness(
    data: CreateBusinessDTO,
    businessId: ObjectId,
    businessOwner: ObjectId
  ): Promise<Response> {
    const business = await this.businessModel.findOne({
      _id: businessId,
      businessOwner,
    });

    if (!business)
      return {
        status: 'failed',
        message: 'business not found',
        data: null,
      };

    const updatedBusiness = await this.businessModel.findOneAndUpdate(
      { _id: businessId },
      { $set: { ...data } },
      { new: true }
    );

    return {
      status: 'success',
      message: 'business data updated',
      data: updatedBusiness,
    };
  }

  async findBusinessByName(businessName: string): Promise<Response> {
    const business = await this.businessModel.findOne({ businessName }).lean();

    if (!business)
      return {
        status: 'failed',
        message: 'business does not exist',
        data: null,
      };

    return {
      status: 'success',
      message: '',
      data: { ...business, businessId: business._id },
    };
  }
}
