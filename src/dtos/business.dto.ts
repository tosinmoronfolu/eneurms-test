import { IsNotEmpty, IsString } from 'class-validator';

export class CreateBusinessDTO {
  @IsNotEmpty()
  @IsString()
  readonly businessName: string;
}
