import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Schema as ms } from 'mongoose';

export type CartDocument = Cart & Document;

@Schema({ timestamps: true })
export class Cart {
  @Prop({ required: true, ref: 'user', type: ms.Types.ObjectId })
  userId: ObjectId;

  @Prop({ required: true, ref: 'product', type: ms.Types.ObjectId })
  productId: string;

  @Prop({ required: true })
  price: number;

  @Prop({ required: true })
  quantity: number;

  @Prop({ required: true })
  subTotal: number;
}

export const CartSchema = SchemaFactory.createForClass(Cart);
