import { UserController } from './../controllers/user.controller';
import { User } from 'src/schemas/user.schema';
import { UserSchema } from './../schemas/user.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { UserService } from './../services/user.service';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  ],
  providers: [UserService],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule {}
