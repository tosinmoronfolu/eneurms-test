import { UserToken, UserTokenDocument } from './../schemas/userToken.schema';
import { MailService } from './mail.service';
import { CreateBusinessDTO } from './../dtos/business.dto';
import { BusinessService } from './business.service';
import { ForgotPasswordDTO, ResetPasswordDTO } from './../dtos/user.dto';
import { Token, TokenDocument } from './../schemas/resetToken';
import {
  hashPassword,
  comparePassword,
  Response,
  createToken,
} from 'src/utils';
import { UserService } from './user.service';
import { CreateUserDTO } from '../dtos/user.dto';
import { UserDocument } from '../schemas/user.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from 'src/schemas/user.schema';
import { Model, ObjectId } from 'mongoose';
import { JwtService } from '@nestjs/jwt';

Injectable();
export class AuthService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
    @InjectModel(Token.name)
    private readonly tokenModel: Model<TokenDocument>,
    private readonly userService: UserService,
    private jwtService: JwtService,
    private businessService: BusinessService,
    private mailService: MailService,
    @InjectModel(UserToken.name)
    private readonly userTokenModel: Model<UserTokenDocument>
  ) {}

  async register(userData: CreateUserDTO): Promise<Response> {
    const userExists = await this.userModel.findOne({ email: userData.email });

    if (userExists) {
      return {
        status: 'failed',
        message: 'email address already exists',
        data: null,
      };
    }

    const password = hashPassword(userData.password);

    const user = await this.userModel.create({ ...userData, password });

    let business;

    if (user.type == 'merchant') {
      business = await this.businessService.createBusiness(
        userData as unknown as CreateBusinessDTO,
        user._id
      );
    }

    const payload = {
      userId: user._id.toString(),
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      domain: user.domain,
      phone: user.phone,
      type: user.type,
      businessId: business != null ? business._id : '',
    };

    const accessToken = await this.jwtService.sign(payload);
    const refreshToken = await this.jwtService.sign(
      { userId: payload.userId },
      {
        expiresIn: parseInt(
          process.env.JWT_REFRESH_TOKEN_EXPIRY_IN_SECONDS,
          10
        ),
      }
    );
    const expiresAt = new Date(
      parseInt(process.env.JWT_TOKEN_EXPIRY_IN_SECONDS, 10)
    );

    const token = createToken(5);

    await this.tokenModel.create({
      userId: payload.userId,
      token,
      expiresAt,
    });

    await this.mailService.sendMail(
      payload.email,
      process.env.SENDGRID_SENDER_EMAIL,
      'test',
      `your token is ${token}`
    );

    await this.userTokenModel.create({
      userId: payload.userId,
      accessToken,
      refreshToken,
      expiresAt,
    });

    return {
      status: 'success',
      message: 'user account created successfully',
      data: { accessToken, refreshToken, ...payload },
    };
  }

  async login(body: User): Promise<Response> {
    const { data: user } = await this.userService.findOne(
      body.email.toLowerCase()
    );

    const payload = {
      userId: user._id.toString(),
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      domain: user.domain,
      phone: user.phone,
      type: user.type,
    };

    let accessToken, refreshToken;

    const userToken = await this.userTokenModel.findOne({
      user: payload.userId,
      expiresAt: {
        $gte: new Date(),
      },
      isExpired: false,
    });

    if (!userToken) {
      accessToken = await this.jwtService.sign(payload);
      refreshToken = await this.jwtService.sign(
        {
          userId: payload.userId,
        },
        {
          expiresIn: parseInt(
            process.env.JWT_REFRESH_TOKEN_EXPIRY_IN_SECONDS,
            10
          ),
        }
      );

      const expiresAt = new Date(
        parseInt(process.env.JWT_TOKEN_EXPIRY_IN_SECONDS, 10)
      );

      await this.userTokenModel.create({
        userId: payload.userId,
        accessToken,
        refreshToken,
        expiresAt,
      });
    }

    return {
      status: 'success',
      message: 'user logged in successfully',
      data: { accessToken, refreshToken, ...payload },
    };
  }

  async validateUser(email: string, password: string): Promise<User> {
    const { data: user } = await this.userService.findOne(email);

    if (user && comparePassword(password, user.password)) {
      return user;
    }

    return null;
  }

  async forgotPassword(body: ForgotPasswordDTO): Promise<Response> {
    const email = body.email.toLowerCase();
    const { data: userWithEmail } = await this.userService.findOne(email);

    if (!userWithEmail)
      return {
        status: 'failed',
        message: 'user account not found',
        data: null,
      };

    const tokenExpiryInSeconds = 300;
    const expiresAt = new Date(
      new Date().getTime() + 1000 * tokenExpiryInSeconds
    );

    const token = createToken(5);

    await this.tokenModel.create({
      userId: userWithEmail._id,
      token,
      expiresAt,
    });

    return {
      status: 'success',
      message: '',
      data: { token },
    };
  }

  async resetPassword(
    body: ResetPasswordDTO,
    token: string
  ): Promise<Response> {
    const now = new Date();

    const isValidToken = await this.tokenModel.findOne({
      token,
    });

    if (!isValidToken)
      return {
        status: 'failed',
        message: 'invalid token',
        data: null,
      };

    if (
      new Date(isValidToken.expiresAt) < now ||
      isValidToken.isExpired == true
    ) {
      await this.tokenModel.findOneAndUpdate(
        { token },
        { $set: { isExpired: true } },
        { new: true }
      );

      return {
        status: 'failed',
        message: 'token has expired',
        data: null,
      };
    }

    const user = await this.userModel.findOneAndUpdate(
      { _id: isValidToken.userId },
      { $set: { password: hashPassword(body.password) } },
      { new: true }
    );

    await this.tokenModel.findOneAndUpdate(
      { token, userId: isValidToken.userId },
      { $set: { isExpired: true } }
    );

    const payload = {
      userId: user._id.toString(),
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      domain: user.domain,
      phone: user.phone,
      type: user.type,
    };

    return {
      status: 'success',
      message: 'password changed successfully',
      data: { ...payload },
    };
  }

  async activateAccount(token: string): Promise<Response> {
    const now = new Date();

    const isValidToken = await this.tokenModel.findOne({
      token,
      expiresAt: {
        $gte: now,
      },
      isExpired: false,
    });

    if (!isValidToken) {
      await this.tokenModel.findOneAndUpdate(
        { token },
        { $set: { isExpired: true } },
        { new: true }
      );

      return {
        status: 'failed',
        message: 'invalid token',
        data: null,
      };
    }

    const user = await this.userModel.findOneAndUpdate(
      { _id: isValidToken.userId },
      { $set: { isActive: true } },
      { new: true }
    );

    await this.tokenModel.findOneAndUpdate(
      { token, userId: isValidToken.userId },
      { $set: { isExpired: true } }
    );

    const payload = {
      userId: user._id.toString(),
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      domain: user.domain,
      phone: user.phone,
      type: user.type,
    };

    return {
      status: 'success',
      message: 'account activated successfully',
      data: { ...payload },
    };
  }

  async googleLogin(request: any): Promise<Response> {
    if (!request.user) {
      return {
        status: 'failed',
        message: 'No user from google',
        data: null,
      };
    }

    const user = await this.userModel.findOne({ email: request.user.email });

    if (!user) {
      return {
        status: 'success',
        message: '',
        data: request.user,
      };
    }

    const payload = {
      userId: user._id.toString(),
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      domain: user.domain,
      phone: user.phone,
      type: user.type,
    };

    let accessToken, refreshToken;

    const userToken = await this.userTokenModel.findOne({
      user: payload.userId,
      expiresAt: {
        $gte: new Date(),
      },
      isExpired: false,
    });

    if (!userToken) {
      accessToken = await this.jwtService.sign(payload);
      refreshToken = await this.jwtService.sign(
        {
          userId: payload.userId,
        },
        {
          expiresIn: parseInt(
            process.env.JWT_REFRESH_TOKEN_EXPIRY_IN_SECONDS,
            10
          ),
        }
      );

      const expiresAt = new Date(
        parseInt(process.env.JWT_TOKEN_EXPIRY_IN_SECONDS, 10)
      );

      await this.userTokenModel.create({
        userId: payload.userId,
        accessToken,
        refreshToken,
        expiresAt,
      });
    }

    return {
      status: 'success',
      message: 'user logged in successfully',
      data: { accessToken, refreshToken, ...payload },
    };
  }

  async resendToken(userId: ObjectId, email: string): Promise<Response> {
    const token = createToken(10);

    const tokenExpiryInSeconds = 300;
    const expiresAt = new Date(
      new Date().getTime() + 1000 * tokenExpiryInSeconds
    );

    await this.tokenModel.create({
      userId: userId,
      token,
      expiresAt,
    });

    await this.mailService.sendMail(
      email,
      process.env.SENDGRID_SENDER_EMAIL,
      'test',
      `your token is ${token}`
    );

    return {
      status: 'success',
      message: 'token sent successfully',
      data: null,
    };
  }

  async refreshAccessToken(
    userId: ObjectId,
    refreshToken: string
  ): Promise<Response> {
    const userToken = await this.userTokenModel.find({ userId, refreshToken });

    if (userToken) {
      const user = await this.userModel.findOne({ _id: userId });

      const payload = {
        userId: user._id.toString(),
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        domain: user.domain,
        phone: user.phone,
        type: user.type,
      };

      const accessToken = await this.jwtService.sign(payload);

      await this.userTokenModel.findOneAndUpdate(
        { userId, refreshToken },
        { $set: { accessToken } },
        { new: true }
      );

      return {
        status: 'success',
        message: '',
        data: accessToken,
      };
    }

    return {
      status: 'failed',
      message: '',
      data: null,
    };
  }
}
