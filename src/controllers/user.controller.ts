import { UpdateUserDTO } from './../dtos/user.dto';
import { JwtAuthGuard } from './../guards/jwt-auth.guard';
import { UserService } from '../services/user.service';
import {
  Controller,
  Get,
  Request,
  Headers,
  UseGuards,
  Response,
  Put,
  Body,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Response as SendResponse, sendResponse } from 'src/utils';

@UseGuards(JwtAuthGuard)
@ApiTags('users')
@ApiBearerAuth()
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('profile')
  private async getProfile(@Request() req): Promise<any> {
    return await this.userService.findOne(req.user.email);
  }

  @Put('profile')
  private async updateProfile(
    @Request() req,
    @Body() body: UpdateUserDTO,
    @Response() res
  ): Promise<SendResponse> {
    return sendResponse(
      res,
      200,
      await this.userService.updateProfile(req.user.email, body)
    );
  }
}
