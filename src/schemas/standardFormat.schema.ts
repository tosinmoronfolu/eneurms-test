import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as ms, ObjectId } from 'mongoose';

export type StandardFormatDocument = StandardFormat & Document;

@Schema({ timestamps: true })
export class StandardFormat {
  @Prop({ required: true, ref: 'store', type: ms.Types.ObjectId })
  storeId: ObjectId;

  @Prop({ required: true })
  timezone: string;

  @Prop({ required: true })
  unitSystem: string;

  @Prop({ required: true })
  weightUnit: string;
}

export const StandardFormatSchema =
  SchemaFactory.createForClass(StandardFormat);
