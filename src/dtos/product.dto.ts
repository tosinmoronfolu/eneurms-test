import {
  IsArray,
  IsBoolean,
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';

export class CreateProductDTO {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsPositive()
  @IsNotEmpty()
  @IsNumber()
  regularPrice: number;

  @IsNotEmpty()
  @IsNumber()
  salePrice: number;

  @IsNotEmpty()
  @IsNumber()
  costPrice: number;

  @IsNotEmpty()
  @IsNumber()
  wholeSalePrice: number;

  @IsNotEmpty()
  @IsString()
  sku: string;

  @IsNotEmpty()
  @IsString()
  barcode: string;

  @IsNotEmpty()
  @IsPositive()
  @IsNumber()
  quantity: number;

  @IsNotEmpty()
  @IsBoolean()
  trackQuantity: boolean;

  @IsNotEmpty()
  @IsBoolean()
  sellOutOfStock: boolean;

  @IsOptional()
  @IsArray()
  options: [{ [key: string]: [string | number] }];
}
