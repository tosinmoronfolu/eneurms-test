import { IsOptional, IsString } from 'class-validator';

export class CreateStandardDTO {
  @IsOptional()
  @IsString()
  timezone?: string;

  @IsOptional()
  @IsString()
  unitSystem?: string;

  @IsOptional()
  @IsString()
  weightUnit?: string;
}
