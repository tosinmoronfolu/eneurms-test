import {
  SocialMediaAccount,
  SocialMediaAccountSchema,
} from './../schemas/socials.schema';
import {
  StandardFormat,
  StandardFormatSchema,
} from './../schemas/standardFormat.schema';
import { BusinessSchema, Business } from './../schemas/business.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { StoreController } from './../controllers/store.controller';
import { StoreService } from './../services/store.service';
import { StoreSchema, Store } from './../schemas/store.schema';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Store.name, schema: StoreSchema },
      { name: Business.name, schema: BusinessSchema },
      { name: StandardFormat.name, schema: StandardFormatSchema },
      { name: SocialMediaAccount.name, schema: SocialMediaAccountSchema },
    ]),
  ],
  providers: [StoreService],
  controllers: [StoreController],
  exports: [],
})
export class StoreModule {}
