import { FacebookStrategy } from './../../configs/facebook.strategy';
import { GoogleStrategy } from './../../configs/google.strategy';
import { UserToken, UserTokenSchema } from './../schemas/userToken.schema';
import { MailService } from './../services/mail.service';
import { BusinessSchema } from './../schemas/business.schema';
import { BusinessService } from './../services/business.service';
import { JwtStrategy } from './../../configs/jwt.strategy';
import { Token, TokenSchema } from './../schemas/resetToken';
import { ConfigModule } from '@nestjs/config';
import { LocalStrategy } from './../../configs/local.strategy';
import { UserModule } from './user.module';
import { UserSchema } from './../schemas/user.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthService } from '../services/auth.service';
import { Module } from '@nestjs/common';
import { AuthController } from './../controllers/auth.controller';
import { User } from 'src/schemas/user.schema';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { Business } from 'src/schemas/business.schema';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Token.name, schema: TokenSchema },
      { name: Business.name, schema: BusinessSchema },
      { name: UserToken.name, schema: UserTokenSchema },
    ]),
    UserModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: process.env.JWT_TOKEN_EXPIRY_IN_SECONDS },
    }),
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    LocalStrategy,
    JwtStrategy,
    BusinessService,
    MailService,
    GoogleStrategy,
    FacebookStrategy,
  ],
  exports: [AuthService],
})
export class AuthModule {}
