import { ObjectId } from 'mongoose';
import {
  IsEmail,
  IsEnum,
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  MinLength,
} from 'class-validator';

enum UserType {
  'customer',
  'merchant',
}
export class CreateUserDTO {
  @IsString()
  readonly firstName: string;

  @IsString()
  readonly lastName: string;

  @IsEmail()
  readonly email: string;

  @IsString()
  readonly domain: string;

  @IsNotEmpty()
  @IsPhoneNumber(undefined, {
    message:
      'please provide a correct phone number. Make sure to put your country code',
  })
  readonly phone: number;

  @IsNotEmpty()
  readonly password: string;

  @IsEnum(UserType)
  readonly type: string;

  @IsOptional()
  @IsString()
  readonly businessName?: string;
}

export class UpdateUserDTO {
  @IsString()
  readonly firstName: string;

  @IsString()
  readonly lastName: string;

  @IsEmail()
  readonly email: string;

  @IsString()
  readonly domain: string;

  @IsNumber()
  readonly phone: number;

  @IsEnum(UserType)
  readonly type: string;

  @IsOptional()
  @IsString()
  readonly businessName?: string;
}

export class LoginUserDTO {
  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  readonly password: string;
}

export class ForgotPasswordDTO {
  @IsEmail()
  @IsString()
  readonly email: string;
}

export class ResetPasswordDTO {
  @IsNotEmpty()
  @MinLength(8)
  readonly password: string;
}

export class RefreshTokenDTO {
  @IsMongoId()
  userId: ObjectId;

  @IsString()
  refreshToken: string;
}
