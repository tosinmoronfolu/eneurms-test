import { ObjectId } from 'mongoose';
import { AddToCartDTO } from './../dtos/cart.dto';
import { JwtAuthGuard } from './../guards/jwt-auth.guard';
import { CartService } from './../services/cart.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Request,
  Response,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
import { Response as SendResponse, sendResponse } from 'src/utils';

@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@ApiTags('cart')
@Controller('carts')
export class CartController {
  constructor(private readonly cartService: CartService) {}

  @Post()
  async addToCart(
    @Body() body: AddToCartDTO,
    @Request() req,
    @Response() res
  ): Promise<SendResponse> {
    const userId = req.user.userId;

    return sendResponse(
      res,
      200,
      await this.cartService.addToCart(body, userId)
    );
  }

  @ApiParam({
    name: 'productId',
    description: 'ID of the product to be updated in the cart',
  })
  @Put(':productId')
  async updateProductInCart(
    @Body() body: AddToCartDTO,
    @Request() req,
    @Response() res
  ): Promise<SendResponse> {
    const userId = req.user.userId;

    return sendResponse(
      res,
      200,
      await this.cartService.updateProductInCart(body, userId)
    );
  }

  @Get('')
  async getUserCart(@Request() req, @Response() res): Promise<SendResponse> {
    const userId = req.user.userId;

    return sendResponse(res, 200, await this.cartService.getUserCart(userId));
  }

  @ApiParam({
    name: 'productId',
    description: 'ID of the product to be deleted from cart',
  })
  @Delete(':productId')
  async deleteProductInCart(
    @Request() req,
    @Response() res,
    @Param('productId') productId: ObjectId
  ): Promise<SendResponse> {
    const userId = req.user.userId;

    return sendResponse(
      res,
      200,
      await this.cartService.deleteProductInCart(productId, userId)
    );
  }
}
