import { JwtAuthGuard } from './../guards/jwt-auth.guard';
import { ObjectId } from 'mongoose';
import { CreateProductDTO } from './../dtos/product.dto';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Request,
  Response,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
import { ProductService } from 'src/services/product.service';
import { Response as SendResponse, sendResponse } from 'src/utils';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@Controller('products')
@ApiTags('products')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @ApiParam({
    name: 'storeId',
    description: 'The ID of the store the product will be added to',
  })
  @Post('stores/:storeId')
  async createProduct(
    @Request() req,
    @Response() res,
    @Body() body: CreateProductDTO,
    @Param('storeId') storeId: ObjectId
  ): Promise<SendResponse> {
    return sendResponse(
      res,
      201,
      await this.productService.createProduct(body, storeId)
    );
  }

  @ApiParam({
    name: 'productId',
    description: 'The ID of the product to be edited',
  })
  @Put(':productId')
  async editProduct(
    @Request() req,
    @Response() res,
    @Body() body: CreateProductDTO,
    @Param('productId') productId: ObjectId
  ): Promise<SendResponse> {
    return sendResponse(
      res,
      200,
      await this.productService.editProduct(productId, body)
    );
  }

  @ApiParam({
    name: 'productId',
    description: 'The ID of the product',
  })
  @Get(':productId')
  async getProduct(
    @Request() req,
    @Response() res,
    @Param('productId') productId: ObjectId
  ): Promise<SendResponse> {
    return sendResponse(res, 200, await this.productService.findOne(productId));
  }

  @ApiParam({
    name: 'productId',
    description: 'The ID of the product',
  })
  @Delete(':productId')
  async deleteProduct(
    @Request() req,
    @Response() res,
    @Param('productId') productId: ObjectId
  ): Promise<SendResponse> {
    return sendResponse(
      res,
      200,
      await this.productService.deleteProduct(productId)
    );
  }
}
