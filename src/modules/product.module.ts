import { ProductController } from './../controllers/product.controller';
import { ProductService } from 'src/services/product.service';
import { Product, ProductSchema } from './../schemas/product.schema';
import { StoreSchema } from './../schemas/store.schema';
import { Store } from 'src/schemas/store.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Store.name, schema: StoreSchema },
      { name: Product.name, schema: ProductSchema },
    ]),
  ],
  providers: [ProductService],
  controllers: [ProductController],
  exports: [],
})
export class ProductModule {}
